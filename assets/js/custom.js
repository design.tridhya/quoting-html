var numItems = $('#mainStepForm .form-step').length;
var progressBarItems = numItems;
$(".total-steps ul").empty();
for (var i = 0; i < progressBarItems; i++) {
    $(".total-steps ul").append('<li></li>');
    if (i == 0) {
        $(".total-steps ul li").addClass('active');
    }
}
var numItemsCounter = 1;
if (numItemsCounter == 1) {
    $("#prevStep").attr('disabled', 'disabled');
}
$("#submitStep").hide();
$("#thanksSection").hide();
$("#submitStep").click(function () {
    $("#thanksSection").show();
    $("#buttonSection").hide();
    $("#mainStepForm .form-step").hide();
})
$("#nextStep").click(function () {
    var validated = false;
    switch (numItemsCounter) {
        case 1:
            var validation = $(".needValid").valid({
                errorPlacement: function (error, element) {

                    if (element.is(".form-check-input")) {
                        error.append(element.parents('.form-check'));
                    }
                    else { // This is the default behavior 
                        error.insertAfter(element);
                    }
                },
            });
            validated = validation;
            break;
        case 2:
            if ($('.form-step-2 .form-check-input:checked').length == 0) {
                validated = false;
                $('.form-step-2 .error').show();
            }
            else {
                $('.form-step-2 .error').hide();
                validated = true;
            }
            break;
        case 3:
            if ($('.form-step-3 .form-check-input:checked').length == 0) {
                validated = false;
                $('.form-step-3 .step-error.error').show();
            } else if($('#CheckYes').is(":checked")){
                console.log('2');
                $('.form-step-3 .step-error.error').hide();
                $("#websiteURL").attr("required","");
                var validation = $(".needValid").valid({
                    errorPlacement: function (error, element) {
    
                        if (element.is(".form-check-input")) {
                            error.append(element.parents('.form-check'));
                        }
                        else { // This is the default behavior 
                            error.insertAfter(element);
                        }
                    },
                });
                validated = validation;
            }
            else {
                $('.form-step-3 .step-error.error').hide();
                validated = true;
            }
            break;
        case 4:
            if ($('.form-step-4 .form-check-input:checked').length == 0) {
                validated = false;
                $('.form-step-4 .error').show();
            }
            else {
                $('.form-step-4 .error').hide();
                validated = true;
            }
            break;
        case 5:
            if ($('.form-step-5 .form-check-input:checked').length == 0) {
                validated = false;
                $('.form-step-5 .error').show();
            }
            else {
                $('.form-step-5 .error').hide();
                validated = true;
            }
            break;
        case 13:
            if ($('.form-step-13 .form-check-input:checked').length == 0) {
                validated = false;
                $('.form-step-13 .error').show();
            } else if($('#CheckFpd2').is(":checked")){
                console.log('2');
                $('.form-step-13 .step-error.error').hide();
                $("#date-input").show();
                $("#date-input").attr("required","");
                var validation = $(".needValid").valid({
                    errorPlacement: function (error, element) {
    
                        if (element.is(".form-check-input")) {
                            error.append(element.parents('.form-check'));
                        }
                        else { // This is the default behavior 
                            error.insertAfter(element);
                        }
                    },
                });
                validated = validation;
            }
            else {
                $('.form-step-13 .error').hide();
                validated = true;
            }
            break;
        case 14:
            if ($('.form-step-14 .form-check-input:checked').length == 0) {
                validated = false;
                $('.form-step-14 .error').show();
            } else if($('#CheckFpd24').is(":checked")){
                $('.form-step-14 .step-error.error').hide();
                
                $("#date-input2").attr("required","");
                var validation = $(".needValid").valid({
                    errorPlacement: function (error, element) {
    
                        if (element.is(".form-check-input")) {
                            error.append(element.parents('.form-check'));
                        }
                        else { // This is the default behavior 
                            error.insertAfter(element);
                        }
                    },
                });
                validated = validation;
            }
            else {
                $('.form-step-14 .error').hide();
                validated = true;
            }
            break;
        default:
            validated = true;
            break;
    }

    if (numItemsCounter < numItems && validated) {
        numItemsCounter++;
        var nextStep = numItemsCounter;
        var prevStep = numItemsCounter - 1;
        $("#mainStepForm .form-step-" + nextStep).css("display", "block");
        $("#mainStepForm .form-step-" + prevStep).css("display", "none");
        $("#prevStep").removeAttr('disabled', 'disabled');
        if (numItemsCounter == numItems) {
            $("#nextStep").hide();
            $("#submitStep").show();
        }
        var mainStepItem = $('.main-steps ul li').length;
        for (var i = 1; i < mainStepItem; i++) {
            var stepData = $(".main-steps ul li").eq(i).attr("step-data");
            if (stepData == numItemsCounter) {
                $('.main-steps ul li').removeClass("active");
                $('.main-steps ul li[step-data=' + stepData + ']').addClass("active");
            }
        }
        $('.total-steps ul li').eq(numItemsCounter - 1).addClass("active");
    }
    else {
        console.log('Reached');
    }
});
$("#prevStep").click(function () {
    if (numItemsCounter < numItems + 1 && numItemsCounter > 1) {
        $('.total-steps ul li').eq(numItemsCounter - 1).removeClass("active");
        $("#mainStepForm .form-step-" + numItemsCounter).css("display", "none");
        var prevStep = numItemsCounter - 1;
        $("#mainStepForm .form-step-" + prevStep).css("display", "block");
        numItemsCounter--;
        if (numItemsCounter == 1) {
            $("#prevStep").attr('disabled', 'disabled');
        }
        if (numItemsCounter < numItems + 1) {
            $("#nextStep").show();
            $("#submitStep").hide();
        }
    }
    else {
        console.log('Reached');
    }
});
$("#CheckFpd24").click(function (){
    $("#date-input2").show();
});
$("#CheckFpd14").click(function (){
    $("#date-input2").hide();
});
$("#CheckFpd2").click(function (){
    $("#date-input").show();
});
$("#CheckFpd1").click(function (){
    $("#date-input").hide();
});
const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))


// Fetch all the forms we want to apply custom Bootstrap validation styles to
const forms = document.querySelectorAll('.needs-validation')

// Loop over them and prevent submission
Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
        }

        form.classList.add('was-validated')
    }, false)
})